package net.cassite.pure.ioc.annotations;

import java.lang.annotation.*;

/**
 * Invoke a method on construction.
 * 
 * @author wkgcass
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Invoke {
}
