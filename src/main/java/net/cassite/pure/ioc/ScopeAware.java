package net.cassite.pure.ioc;

/**
 * suggest that the weaver is aware of ioc session
 *
 * @author wkgcass
 * @since 0.2.1
 */
public interface ScopeAware {
        void setScope(Scope scope);
}
