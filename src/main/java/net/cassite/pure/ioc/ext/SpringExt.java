package net.cassite.pure.ioc.ext;

import net.cassite.pure.ioc.ExtendingHandler;
import net.cassite.pure.ioc.annotations.Wire;
import org.springframework.context.ApplicationContext;

/**
 * extension for spring. retrieve instances from spring by bean names
 */
public class SpringExt implements ExtendingHandler {
        @Wire
        private ApplicationContext context;

        @Override
        public Object get(String[] args) {
                return context.getBean(args[0]);
        }
}
