package net.cassite.ioc.TestIoC;

/**
 * destroy weaver
 */
public class DestroyWeaver implements net.cassite.pure.aop.DestroyWeaver<DestroyBean> {
        @Override
        public void doDestroy(DestroyBean target) {
                target.destroy();
        }
}
