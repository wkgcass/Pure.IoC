package net.cassite.ioc.TestIoC;

import net.cassite.pure.ioc.AutoWire;
import net.cassite.pure.ioc.annotations.Force;
import net.cassite.pure.ioc.annotations.Wire;

/**
 * test field injection
 */
public class FieldInjectBean extends AutoWire {
        @Wire
        @Force("a")
        String string;
        @Wire
        @Force("1")
        int integer;
}
