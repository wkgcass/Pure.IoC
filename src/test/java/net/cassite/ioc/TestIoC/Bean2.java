package net.cassite.ioc.TestIoC;

import net.cassite.pure.ioc.annotations.ScopeAttr;
import net.cassite.pure.ioc.annotations.Wire;

@Wire
public class Bean2 {
        private BeanA beanA;

        public BeanA getBeanA() {
                return beanA;
        }

        public void setBeanA(@ScopeAttr("beanA") BeanA beanA) {
                this.beanA = beanA;
        }
}
