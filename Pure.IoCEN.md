点击[这里(cassite)](http://blog.cassite.net/JAVA/Pure.IoC)或[这里(git@osc)](http://git.oschina.net/wkgcass/Pure.IoC)获取中文文档。

#Pure.IoC
Pure.IoC is a light-weight type and annotation based dependency injection framework

>based on jdk 1.8    
>It is recommended to use with Spring

```xml
<dependency>
  <groupId>net.cassite</groupId>
  <artifactId>pure.ioc</artifactId>
  <version>0.3.2</version>
</dependency>
```

##Philosophy
Replace **complex configuration** with **logical dependency**.

##Main Features
* IOC
* AOP

##Design idea
Usually we use *Spring* to do IoC management.However, Spring is based on *bean*s, It requires mapping a class to a bean, then describing the dependencies of beans.

In fact, dependency only need to be determined by Type.  
e.g.

	class A{
		...
		public void setB(B b){ this.b=b; }
	}
	
	class B{
	}

It is clear that A depends on B. A requires an instance of B to inject through setB. It's a natural logical relation.

Type dependency would be set when coding. So how great it would be if some tools/frameworks can help with this clear depending relation.  
So, i developed *Pure.IoC*

#Framework of framework
##Expansibility
It took me a lot time thiking how to design *Pure.IoC*.   
Take the following complex situation as an example (Actually it's still not very complex)

	@Singleton
	@Wire
	class Complex{
		private ......
		
		public Complex(){ ... }
		@Default public Complex(AnotherClass obj){ ... }
		
		public void setA(A a){ ... }
		public void setB(B b){ ... }	
		public void setInterface(Interf1 interf){ ... }
		public void setInterface(@Use(clazz=Impl2.class)Interf2 interf){ ... }
	}
	
	@Default(clazz=Impl1.class) interface Interf1{ ... }
	
The annotations described:

* @ Singleton Complex is a singleton
* @ Wire Do wiring when the class goes through AutoWire
* @ Default Choose the default constructor(used on the constructor), determine implement class(used on the interface)
* @ Use Determines which class to use

The annotations should be able to expand infinitely, and it should not cause the system to be more complex.  
Finally I designed the following pattern:

	AnnotationHandler + HandlerChain
AnnotationHandlers are divided into 4 kinds:

* SetterAnnotationHandler Used to invoke setters, takes annotations on setters/correspoinding fields/parameters into consideration.
* ParamAnnotationHandler Used to retrieve instances with given type, takes annotations on parameters into consideration.
* ConstructorFilter Used to select constructors, takes annotations on constructors into consideration.
* TypeAnnotationHandler Used to choose a type, takes annotations on types.

Handler are reigstered into IOCController, and do handling operations by the order when registering. The implements of handlers looks like AOP, usually it will invoke chain.next().handle(...) , and do corresponding operations considering the results or exceptions from the next handler.

See handlers' doc for more info.

As a result, it's very convenient to do expansion.

##Circular Dependencies
Though it's not usual to occur circular dependencies，we may see them sometimes. A->B->C->A  
At least one of A,B,C should be singleton, otherwise the constructions would continue to go on.

Spring require constructors not having circular dependencies, otherwise the construction cannot finish.

However Pure.IoC solved the problem cleverly.

* All injection are performed in construction. The construction won't finish before injecting finished.
* For singletons, they hand over their references to IOCController to let the system know the singleton already exist.
* The construction will finish only when injecting finishes, so don't worry that the instantiating may not full。

In a result, the injections can go on smoothly even when constructors have cirular dependencies (Actuall, all dependencies are constructor dependencies in the framework).

##Flexibility
Pure.IoC is designed as injecting when constructing, so no other modules are required when putting this into other frameworks. It's a good idea to use this framework with Spring. Even do injection on some setters with Pure.IoC and some others with Spring.

Also it doesn't require any configuration when using some frameworks like Struts2. The injections are inside the class, so it doesn't need to change object factory of Struts2. (an object itself is a factory)

The recommended way of using this framework:

	extends AutoWire
	
But in some circumstances, the class has already extended another class, there're 2 backup ways:

1.

	@Wire class A { ... }
	
Any class with @Wire annotation would be injected when going through Pure.IoC.

You can use:
	
	AutoWire.get(A.class)

to get instances.

2.

	class A {
		public A(){
			AutoWire.wire(this);
		}
	}
	
Handle over it's reference to framework when constructing.

>Actually all entrances would call AutoWire.wire(Object)

#How to use ?
* First of all, use these 3 methods presented up there to enable the injections.
* Then invoke the following method at entrance of your application.
	
		IOCController.autoRegister();
  or register each handler mannually. e.g.
  
  		register(new DefaultConstructorFilter());
  >if you are writing JavaEE, you can create a Listener to do this job.
* Finally use
		
		IOCController.closeRegistering();
  to close registration of handlers.  
  This step is optional, only to guarenteen handler list won't be modified.
 
Registering no handlers but started wiring would invoke `autoRegister` and `closeRegistering` first

##Default behavior
The default behaviors with no annotation presented are:

When injecting setter, obtain parameter type and request an instance from IOCController. Then invoke the setter.

When trying to get instances, get the only constructor, **or** get the constructor with no parameters.  
If the constructor has parameters, get instances with corresponding types.  
Finally, do constructing.

##Extended Annotations
###Setter

* Use(clazz, constant, variable)  
	**clazz** use designated type as the argument. Then, type check would be invoked.  
	**constant** represents constants registered in IOCController. A constant means an object.  
	**variable** represents variables registered in IOCController. If the method/field is not static, IOCController will retrieve an instance of the object and invoke/get. It's recommended to mark those classes with Singleton.
* Force(value)  
	Transform String value into proper type. Force only support primitives and Strings
* Ignore  
	Ignore the setter
* Extend(handler, args)  
	Retrieve objects from other object factories  
	**handler** ExtendingHandler, name a handler, which describes how to retrieve object from the object factory    
	**args** arguments to fill the handler
* Session(value)  
	argument of the setter would be shared in the same session  
	**value** shared alias  

###Type

* Wire
	indicates the class requires dependency injection.
* Singleton  
	indicates the class is a singleton
* Default(clazz)  
	Usually used on **interface**s or **abstract class**es  
	The constructing target would be redirected when doing Type check.

###Constructor

* Default()  
	indicates the constructor to use when choosing constructor method is ambiguous
	
###Param

* Use(clazz, constant, variable)  
	**clazz** use designated type as the argument. Then, type check would be invoked.  
	**constant** represents constants registered in IOCController.  
	**variable** represents variables registered in IOCController.
* Force(value)  
	Transform String value into proper type. Force only support primitives and Strings
* Extend(handler, args)  
	Retrieve objects from other object factories  
	**handler** ExtendingHandler, name a handler, which describes how to retrieve object from the object factory    
	**args** arguments to fill the handler
* Session(value)  
	argument would be shared in the same session  
	**value** shared alias  
	
>Also, param provide primitive type injection.  
>If no injections are found, it will check primitives and array types, and inject with initial values (0，false，array[length=0])

#Other
##Session
Session exists in the duration of initializing wiring works until finishing the wiring. Almost all methods pass the session as arguments.    
e.g.  
A depends on B, B depends on C and D. Also, A and C extends AutoWire (which implicitly invoked AutoWire.wire(o)), B and D use Wire annotation  
Then, wire work of ABD will share one Session, and wire work of C belongs to another.

##Utils
Pure.IoC provides a Utils tool class, which contains some usual operations, with some methods in IOCControlelr. If it's not convenient to extend IOCController, you can use methods in Utils to invoke those protected methods in IOCController.

##ExtendingHandler
The design intention of designing this is to simplify the work of *retrieving instances from other object factories*. But the usage is not limited to that.  
Actually it can be considered as simplify of *extension*.  
It's used to do handling work of types, setters and params, but the accepted arguments can only be String Arrays.

###SessionAware
For those classes which implement SessionAware, or classes with `setScope(Scope s)` method and had been marked with @Wire, will be invoked with current scope.

#AOP
AOP feature was added since version 0.1.1

present annotation 

	@AOP({Weaver.class,...})

on your class with interfaces, to get support of AOP

When the class is wired by *Pure.IoC* or directly retrieved with

	AOPController.weave(()->Instance of the class,InstanceClass.class)

would wire/retrieve the proxy object.

##How to Use?
###Create your Weaver

	class YourWeaver implements Weaver{
		@Override
		protected void doBefore(AOPPoint point) {
			...
		}

		@Override
		protected void doAfter(AOPPoint point) {
			...
		}

		@Override
		protected void doException(AOPPoint point) throws Throwable {
			...
		}
	}
	
before, after, exception means Before, AfterReturn, AfterThrowing. Simply *extends* Weaver means Around.

BeforeWeaver, AfterWeaver, ExceptionWeaver are also provided, *extends* them if you only need specific tpye of cut point.

It's also very convenient if you need `Introduction`. The only thing you need to do is let your Weaver class *implements* interfaces you want to introduce.

###Weaving
use
	
	@AOP({Weaver1.class, Weaver2.class, ...})
	
to weave in.  
If using cglib, you can set useCglib=true  

	@AOP(value={Weaver1.class, Weaver2.class, ...} useCglib=true)
	
the AOP process follows these steps.

* get Weaver instances with AutoWire.get(Class)
* set a variable to store the current index of the Weaver array.
* do 'before' from the first to the last
* --If the returnValue was set, simply jump to 'after' step.
* invoke method
* --If exception occurred, do 'exception' from last to the first
* do 'after' from the stored index to the first

###AOPPoint
This class contains the following public fields/methods

* target						target object
* method						current method
* args							arguments（can be modified directly）
* returnValue()				retrieve the return value
* returnValue(Object)		set the return value
* exception()					retrieve current exception
* exceptionHandled()			mark that the current exception had been handled

###TargetAware
For those weavers which implement TargetAware, `targetAware(.)` method will be invoked after retrieving instance of the weaver using proxy target as the argument.