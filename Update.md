#更新内容 Update
0.3.1 --> 0.3.2
##BUG
DefaultParamHandler中曾经无法以正确的逻辑处理绑定的类型,现已修复.  
DefaultParamHandler used not able to handle bond types with correct logic, which has been fixed now.

TypeWireHandler在针对被TypeDefaultHandler处理过的对象时会再次执行注入操作(TypeDefaultHandler会另起一条HandlerChain,在新Chain中也存在TypeWireHandler),现已修复  
TypeWireHandler would do wire operations again when handling the object that had been handled by TypeDefaultHandler.(TypeDefaultHandler would start another handlerChain, the new chain contains TypeWireHandler), which has been fixed now.

自动注册可能在调用`AutoWire.get(...)`时失败,现已修复  
Auto register cannot function if first calling `AutoWire.get(...)`, which has been fixed now.

##修改 Modification
现在AOPController.weave,TypeAnnotationHandler,ParamAnnotationHandler,IOCController.get增加了一个参数,用来表示"原本期望的类型"  
Now AOPController.weave,TypeAnnotationHandler,ParamAnnotationHandler,IOCController.get added a parameter, to represent 'type originally expected'

AOPController.weave方法现在接受RFunc0<R>代替原本接收一个对象 e.g. `AOPController.weave(()->obj,obj.getClass())`  
AOPController.weave now accept RFunc0<R> instead of an object

一些文档的修正  
Some revisions on docs

#更新内容 Update
0.2.2 --> 0.3.1
##修改 Modification
Session统一重命名为Scope(包括@Session和SessionAware,现在被重命名为@ScopeAttr和ScopeAware), 原本注册在IOCController中的对象,除Handler外,都需要注册在IOController.rootScope中.  
Session are renamed to Scope(including annotation Session, and SessionAware. they have been renamed into ScopeAttr and ScopeAware). the objects registered into IOCController should be registered into IOCController.rootScope.

SetterAnnotationHandler被移除,setter的调用全部由ParamAnnotationHandler接管  
SetterAnnotationHandler is removed, invocation of setters are taken over by ParamAnnotationHandler

Use注解更改,现在改为@Use(value:String,cls:Class).value/cls两者取其一.都存在则以value为准.value表示在scope中注册的值.clazz表示重新定位到对应类型  
Use annotation is modified. Now it's @Use(value:String,cls:Class). only one of value/cls would function. if both exists, use `value`. `value` means value registered in scope. `cls` means redirect to given type.

Default注解更改,现在改为@Default(value:Class). `value`表示需要实例化的类型.
Default annotation is modified. Now it's @Default(value:Class). `value` means the type redirected to.

所有注解都增加了@Inherited
All annotations are added @Inherited

ScopeAware接口移动至ioc包中,并改为使用setter注入Scope.实际上,只要类型为Scope均可以实现Scope的注入.

必须有Wire注解才会执行注入.若@Wire标注在类型上,则会对所有setter以及已指定的field进行注入.其他情况只注入指定的setter和field
Injection would take place only when @Wire is presented. If @Wire is presented on type, all setters and fields with @Wire will be injected, otherwise only inject setters and fields.

##新功能 New Features
现在支持直接对Field进行的注入,只需在字段上加上@Wire注解
Now it supports injecting directly into fields, the only thing needed is add @Wire on fields

现在可以不对类上增加@Wire注解,而在字段或setter上增加@Wire注解,或是在方法上加上@Invoke注解,将只对选中的字段或setter进行装配,或对方法进行调用.
Now there's no need to add @Wire on classes, but add @Wire on fields or setters, or add @Invoke on methods. then it would wire the selected fields or setters, or invoke the selected methods

@ScopeAttr注解可指定`thread=true`,表示值将加入Scope.currentThreadScope()中  
@ScopeAttr annotation can assign `thread=true`, which means add value into Scope.currentThreadScope()

#更新内容 Update
0.2.1 -->0.2.2
##BUG
曾经@Force不能正确判断非基本类型。现已修复。  
@Force used to be unable to inject non-primitives. It's been fixed now.

AOPController中有一个实现失误.现已修复.  
There used to be a bug in AOPController. It's been fixed now.

曾经Type和Parameter不能判断自动装箱类型。现已修复  
Type and Parameter used to be unable to retrieve auto-box types. It's been fixed now.

##修改 Modification
IOCController中的几个方法被标注为`@Deprecated`  
A few methods in IOCController are marked as `@Deprecated`

* registerVariable(String,Field)
* registerVariable(String,FieldSupport)
* registerVariable(String,Method)
* registerVariable(String,MethodSupport)

使用`registerVariable(String,RFunc1)`代替之  
Use `registerVariable(String,RFunc1)` instead

没有重载doException方法的Weaver默认行为被修改为`抛出异常`,但Weaver和LoggedWeaver被视为要自定义doException方法,不作改动  
Default behavior of Weavers that don't override doException will be changed to `throwing exception`,Weaver and LoggedWeaver are considered as customizing `doException`, which are not effected

##新功能 New Features
现在可以向IOCController注册`Map<Object,Object>`对象(Properties的父类型)，并通过@Force注解的新属性`properties`协同`value`指定要注入的property  
Now you can register `Map<Object,Object>` objects into IOCController. And force the injected value using `properties` and `value` defined in @Force

例如 e.g.

properties

```properties
address=127.0.0.1
port=23333
```

java

```java
Properties config=new Properties();
config.load(X.class.getResourceAsStream("/config.properties");
IOCController.registerProperties("config",config);

@Force(properties="config", value="address")
public void setAddress(String address) ...

@Force(properties="config", value="port")
public void setPort(int port) ...
```

如果在开始执行注入时没有注册任何handler,则会自动调用`autoRegister`并执行`closeRegistering`
`autoRegister` and `closeRegistering` would be invoked if injection has started but no handlers registered.

#更新内容 Update
0.1.3 --> 0.2.1
##BUG
曾经@Singleton注解的类不可使用Wire注解(其它两种方式均可用).现已经修复.  
classes annotated by @ Singleton cannot use annotation @ Wire. It has been fixed now.

##修改 Modification
现在新增了Session,它贯穿于一次调用AutoWire.wire(o)的装配工作.详见文档.
Now Session has been added, it goes through one wiring work invoking AutoWire.wire(o). See doc for more info.

Weaver曾是一个类,织入同时提供日志服务.现在重命名为LoggedWeaver,并提供Weaver接口进行织入操作  
Weaver used to be a class, which provides logging while weaving. Now it's renamed into LoggedWeaver, and an interface Weaver is provided to do weaving operations.

##新功能 New Features
对于实现了TargetAware接口的Weaver,在获取Weaver实例后将以被代理对象作为参数调用该方法.
For those weavers which implements an interface named 'TargetAware', the method will be invoked after retrieving instance of the weaver using proxy target as the argument.

对于实现了SessionAware接口的Weaver,在获取Session实例后将以当前Session为参数调用该方法.
For those weavers which implements an interface named 'SessionAware', the method will be invoked after retrieving instance of the weaver using current ioc session.

##调整 Adjust
对于没有接口的对象,自动使用cglib,无需再显式设置useCglib=true  
for those objects without interfaces, cglib is automatically used, no need for useCglib=true on annotations

一些遗留的log4j的拼接模式换成了slf4j预留空位的模式.  
some old string combining in log4j are changed to space leaving {} pattern in slf4j.

一些文档和日志拼写修正  
some revision about doc and log

#更新内容 Update
0.1.2 --> 0.1.3
##调整 Adjust
提供Maven支持  
maven support provided

Logger使用slf4j  
use slf4j as Logger library

采用新版本Style函数式工具集  
use the new version of Style functional toolbox

变量采用Supplier进行管理.曾经的Method和Field同样支持  
`variable` is managed using `Supplier`. Method and Field are also supported.

#更新内容 Update
0.1.1 --> 0.1.2
##新功能 New Features
AOP现在可以支持Cglib
AOP now supports Cglib
在AOP注解上增加
Add this to AOP annotation

	useCglib＝true

#更新内容 Update
0.0.2 --> 0.1.1
##BUG
Extend注解曾有一个编码过程中的失误，导致注解不可用。现已修复  
Extend annotation used to occur a programming fault, which leads to the annotation unable to use. Now the bug has been fixed.

##调整 Adjust
使用Style工具集时曾使用继承的方式，现在全部改为

	import static net.cassite.style.?.*

的方式。  
The way of using Style toolbox used to be *extend*ing, now it's changed to

	import static net.cassite.style.?.*

##新功能 New Features
AOP  
现在您可以使用Pure.IoC来进行AOP。  
Now you can use *Pure.IoC* to do AOP

给您**带接口的**类型增加@AOP({Class<Weaver>,...})注解，然后被Pure.IoC注入时，或者直接使用AOPController.weave(该类实例)即可注入/获取代理对象。  
Present an AOP({Class<Weaver>,...}) annotation to your type with interface(s), then when it's been wired by Pure.IoC or directedly invoking AOPController.weave(*Instance of the type*) would wire/get the proxy object.


查看文档以获取更多信息  
See document for more info

------

0.0.1 --> 0.0.2

##调整 Adjust
IOCController方法全部改为*static*，不会对之前版本扩展造成任何影响  
Methods in IOCController are modified with *static*, no effects on extensions of earlier version(s).

##新功能 New Features
新工具类Utils  
A new tools class Utils

Extend注解和其对应Handler  
Extend Annotation, and its supporting handlers

查看文档以获取更多信息  
See document for more info